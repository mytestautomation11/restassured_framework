package TestScripts;

import java.util.List;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Get_TestScriptretry extends API_Trigger {
	
public static void execute()
	
	{
	
	int exp_page = 2;
	int exp_per_page = 6;
	int exp_total = 12;
	int exp_total_pages = 2;
	
	//declare the expected results from data array
	
	int[] exp_id = {7, 8, 9, 10, 11, 12};
	
	String[] exp_email = {"michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
			"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in"};
	
	String[] exp_first_name = {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
	
	String[] exp_last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
	
	String[] exp_avatar = {"https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg", "https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg"};
	
	//Declare expected results of support
	String exp_url = "https://reqres.in/#support-heading";
	String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";
	
		
	Response response = Get_API_Trigger(get_requestbody(),get_endpoint());
	
	//Extract the status the code
	int statuscode = response.statusCode();
	System.out.println(statuscode);
	
	// Fetch the response body parameters
	ResponseBody responsebody = response.getBody();
	System.out.println(responsebody.asString());

	// Fetch page parameters

		int res_page = responsebody.jsonPath().getInt("page");
		int res_per_page = responsebody.jsonPath().getInt("per_page");
		int res_total = responsebody.jsonPath().getInt("total");
		int res_total_pages = responsebody.jsonPath().getInt("total_pages");
			
	//Fetch Size of data array
		List<String> dataArray = responsebody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();
	
	//Validation

	// Validate per page parameters
		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);
		
		// Validate data array
		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(Integer.parseInt(responsebody.jsonPath().getString("data[" + i + "].id")), exp_id[i],
					"Validation of id failed for json object at index : " + i);
			Assert.assertEquals(responsebody.jsonPath().getString("data[" + i + "].email"), exp_email[i],"Validation of email failed for json object at index : " + i);
			Assert.assertEquals(responsebody.jsonPath().getString("data[" + i + "].first_name"), exp_first_name[i],"Validation of first_name failed for json object at index : " + i);
			Assert.assertEquals(responsebody.jsonPath().getString("data[" + i + "].last_name"), exp_last_name[i],"Validation of last_name failed for json object at index : " + i);
			Assert.assertEquals(responsebody.jsonPath().getString("data[" + i + "].avatar"), exp_avatar[i],"Validation of avatar failed for json object at index : " + i);
		}
		
		// Step 7.3 : Validate support json
		Assert.assertEquals(responsebody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");
		Assert.assertEquals(responsebody.jsonPath().getString("support.text"), exp_text, "Validation of URL failed");
		
	}



}
