package TestScripts;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utilities;
import io.restassured.response.Response;

public class Delete_TestScript extends API_Trigger {
	
	public static void execute() throws IOException
	{
		File logfolder = Utilities.createfolder("Delete_API");
				
		Response response = Delete_API_Trigger(delete_requestbody(),delete_endpoint());
		int status_code = response.statusCode();
		System.out.println(status_code);
		
		for(int i=0; i<5; i++) {
			
			if(status_code==204) {
				Utilities.createLogFile("Delete_API_TC1", logfolder, delete_endpoint(), delete_requestbody(),
						response.getHeaders().toString(), response.getBody().asString());
				break;
			}
			else {
				System.out.println("Status code for iteration " +i + "is: " + status_code + ", which is not equal to the expected status code hence retrying the API");
			}
			
			Assert.assertEquals(status_code, 204, "Correct status code not found even after retrying for 5 times");
		}
		
		
	}

}
