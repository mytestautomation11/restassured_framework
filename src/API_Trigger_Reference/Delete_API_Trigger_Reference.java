package API_Trigger_Reference;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class Delete_API_Trigger_Reference {

	public static void main(String[] args) {
		
		String hostname = "https://reqres.in/";
		String resource = "/api/users/2";
		
		
		//Configure the API and trigger
		RestAssured.baseURI = hostname;
		
		//String responseBody=given().when().delete(resource).then().extract().response().asString();
		
		//System.out.println(responseBody);
		
		given().log().all().when().delete(resource).then().log().all().extract().response().asString();

	}

}
