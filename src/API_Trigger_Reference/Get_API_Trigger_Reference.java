package API_Trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;


public class Get_API_Trigger_Reference {

	public static void main(String[] args) {
		//Collect all needed information and save it into local variables
				String hostname = "https://reqres.in/";
				String resource = "/api/users?page=2";
				
				//Declare the expected page parameters
				int exp_page = 2;
				int per_page = 6;
				int total = 12;
				int total_pages = 2;
				String exp_url = "https://reqres.in/#support-heading";
				String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";
				
				//Declare the expected data array parameters
				
				int[] id = {7,8,9,10,11,12};
				String[] email = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
				String[] first_name= {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
				String[] last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
				String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};
				
				//Declare BaseURI
				RestAssured.baseURI = hostname;
				
				//Configure the API for execution and save the response in a String variable
				String responseBody = given().when().get(resource).then().extract().response().asString();
				System.out.println(responseBody);
				
				//parse the response body using JsonPath
				//Create the object of JsonPath
				JsonPath jp = new JsonPath(responseBody);
				
				//Fetch the response body data array length
				int size = jp.getInt("data.size()");
				System.out.println("Length of array : " + size);
				
				
				int res_page = jp.getInt("page");
				int res_perpage = jp.getInt("per_page");
				int res_totalpages = jp.getInt("total_pages");
				int  res_total = jp.getInt("total");
				String res_support_url = jp.getString("support.url");
				String res_support_text = jp.getString("support.text");

				
				for (int i=0; i<size ; i++)
				{
					//Fetch expected results
					String exp_id = Integer.toString(id[i]) ;
					//System.out.println("Expected id : " + exp_id);
					String exp_email = email[i];
					//System.out.println("Expected email: " + exp_email);
					String exp_firstname = first_name[i];
				    String exp_lastname = last_name[i];
				    String exp_avatar = avatar[i];
					
				    //Fetch response body parameters
				    //Parse individual parameters using jsp_res object
					String res_id= jp.getString("data["+i+"].id");
					//System.out.println("Response body id: " + res_id);
					String res_email= jp.getString("data["+i+"].email");
					//System.out.println("Response body email: " + exp_email);
					String res_firstname= jp.getString("data["+i+"].first_name");
					String res_lastname= jp.getString("data["+i+"].last_name");
					String res_avatar= jp.getString("data["+i+"].avatar");
								
					//Validating using TestNG assertions
					
					Assert.assertEquals(exp_page, res_page);
					Assert.assertEquals(res_total, total);
			        Assert.assertEquals(res_totalpages, total_pages);
			        Assert.assertEquals(res_perpage,per_page);
			        Assert.assertEquals(res_support_url,exp_url);
			        Assert.assertEquals(res_support_text,exp_text);
					Assert.assertEquals(res_id, exp_id , "id assertion fail");
					Assert.assertEquals(res_email, exp_email,"email assertion fail");
					Assert.assertEquals(res_firstname, exp_firstname, "firstname assertion fail");
					Assert.assertEquals(res_lastname, exp_lastname,"lastname assertion fail");
					Assert.assertEquals(res_avatar, exp_avatar, "avatar assertion fail");
					
				}

	}

}
