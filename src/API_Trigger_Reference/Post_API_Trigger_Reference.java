package API_Trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_API_Trigger_Reference {

	public static void main(String[] args) {
		String hostname = "https://reqres.in/";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		
		//Configure the API and trigger
		RestAssured.baseURI = hostname;
		
		String responseBody=given().header(headername,headervalue).body(requestBody).when().post(resource).then().extract().response().asString();
		System.out.println(responseBody);
		// Step 5 : Parse the response body
		
				// Step 5.1 : Create the object of JsonPath
				
				JsonPath jsp_res = new JsonPath(responseBody);
				
				// Step 5.2 : Parse individual params using jsp_res object 
				
				String res_name=jsp_res.getString("name");
				System.out.println(res_name);
				String res_job=jsp_res.getString("job");
				System.out.println(res_job);
				String res_id=jsp_res.getString("id");
				System.out.println(res_id);
				String res_createdAt=jsp_res.getString("createdAt");
				res_createdAt = res_createdAt.substring(0, 11);
				System.out.println(res_createdAt);
				
				// Step 6 : Validate the response body
				
				// Step 6.1 : Parse request body and save into local variables
				
				JsonPath jsp_req = new JsonPath(requestBody);
				String req_name = jsp_req.getString("name");
				String req_job = jsp_req.getString("job");
				
				// Step 6.2 : Generate expected date
				
				LocalDateTime currentdate = LocalDateTime.now();
				String expecteddate = currentdate.toString().substring(0, 11);
				
				// Step 6.3 : Use TestNG's Assert
				
				Assert.assertEquals(res_name, req_name, "Name in the responsebody is not equals to Name in the request body");
				Assert.assertEquals(res_job, req_job, "Job in the responsebody is not equals to Job in the request body");
				Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
				Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");
	}
	
	
}
