package Request_Specification_API_Trigger;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Patch_Request_Specification {

	public static void main(String[] args) {
		
		//Fetch the needed parameters
				String hostname = "https://reqres.in";
				String resource = "/api/users/2";
				String headername = "Content-Type";
				String headervalue = "application/json";
				String requestbody = "{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"Developer\"\r\n"
						+ "}";
				
				//build the req specs using req specs
				
				RequestSpecification req_specs = RestAssured.given();
				
				//Set the header
				
				req_specs.header(headername, headervalue);
				
				//Set the request body
				req_specs.body(requestbody);
				
				//Trigger the API
				
				Response response = req_specs.patch(hostname + resource);
				
				//Extract the status code
				
				int statusCode = response.statusCode();
				System.out.println(statusCode);
				
				//Fetch the responsebody parameters
				
				ResponseBody responsebody = response.getBody();
				System.out.println(responsebody.asString());
				
				String res_name = responsebody.jsonPath().getString("name");
				String res_job = responsebody.jsonPath().getString("job");
				String res_updatedAt = responsebody.jsonPath().getString("updatedAt");
				res_updatedAt = res_updatedAt.toString().substring(0,11);
				
				//Fetch the request body parameters
				
				JsonPath jsp_req = new JsonPath(requestbody);
				String req_name = jsp_req.getString("name");
				String req_job = jsp_req.getString("job");
				
				//Generate the expected date
				
				LocalDateTime currentdate = LocalDateTime.now();
				String expecteddate = currentdate.toString().substring(0,11);
				
				//Validate using TestNG
				
				Assert.assertEquals(res_name, req_name, "Name in reponse body not equals to name in request body");
				Assert.assertEquals(res_job, req_job, "Job in response body is not equals to job in request body");
				Assert.assertEquals(res_updatedAt, expecteddate, "updatedAt in responsebody is not equal to date generated");

	}

}
