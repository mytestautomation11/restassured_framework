package Request_Specification_API_Trigger;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Put_Request_Specs {

	public static void main(String[] args) {
		
		
		//Fetch the needed parameters
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestbody = "{\r\n"
				+ "    \"name\": \"Roshni\",\r\n"
				+ "    \"job\": \"Tester\"\r\n"
				+ "}";
		
		//Trigger the API
		
		//build the request specification using request specification
		
		RequestSpecification req_specs = RestAssured.given();
		
		//Set the header
		
		req_specs.header(headername,headervalue);
		
		//Set the request body
		
		req_specs.body(requestbody);
		
		//Trigger the API
		Response response = req_specs.put(hostname + resource);
		
		//Extract the status code
		
		int statuscode = response.getStatusCode();
		System.out.println(statuscode);
		
		//Fetch the response body parameters
		
		ResponseBody responsebody = response.getBody();
		System.out.println(responsebody.asString());
		
		String res_name = responsebody.jsonPath().getString("name");
		String res_job = responsebody.jsonPath().getString("job");
		String res_updatedAt = responsebody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0,11);
		
		//Fetch the requestbody parameters
		
		JsonPath jsp_req = new JsonPath(requestbody);
		
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0,11);
		
		//validate using TestNG
		
		Assert.assertEquals(res_name, req_name, "Name in the response body is not equal to name in request body");
		Assert.assertEquals(res_job, req_job, "Job in the response body is not equal to job in the request body");
		Assert.assertEquals(res_updatedAt, expecteddate, "updatedAt date is not equals to expected date");
		
			

	}

}
