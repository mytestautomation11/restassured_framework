Endpoint is :
https://reqres.in/api/users

Request body is :
{
"name": "morpheus",
    "job": "leader"
}

Response header is :
Date=Sun, 28 Apr 2024 02:02:33 GMT
Content-Type=application/json; charset=utf-8
Content-Length=78
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1714269753&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=Jmp93TesuX5fkRh3qXWIG9IpT5xJ7yOmChaiQhiPOsc%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1714269753&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=Jmp93TesuX5fkRh3qXWIG9IpT5xJ7yOmChaiQhiPOsc%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"4e-EJF2nCeWsPTcFbrx+kqUeQo8LnM"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=87b378877d419a96-NAG

Response body is :
{"name":"John","job":"10.0","id":"498","createdAt":"2024-04-28T02:02:33.750Z"}